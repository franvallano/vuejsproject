/***** components *****/

Vue.component('todo-item',{
  props:['todo'],
  template: '<li>{{todo.text}}</li>'
})


/***** App *****/

// v-on equivale a @ -> v-on:click = @click
// v equivale a : -> v-bind = :bind
var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        messageWithDate: 'You loaded this page on' + new Date().toLocaleString(),
        list:[{id:0, text:'this is first line'}],
        index:1
    },
    methods: {
      addText: function(cad){
        var ident = app.index++;
        addData(ident, app.list, cad);
      }
    }
});


/***** Functions *****/

function addData(index, list, cad){
  list.push({id: index, text: cad })
}
